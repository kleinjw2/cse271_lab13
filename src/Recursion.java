/**
 * A simple program of two basic recursive methods. Practice using GitLab.
 * 
 * @author Kleinjw2
 *
 */
public class Recursion {

    public static void main(String[] args) {
        System.out.println(powerN(5, 3));
        System.out.println(triangle(6));
    }

    /**
     * Recursive method to calculate the power of a number.
     * 
     * @param base the number being raised to a power n
     * @param n    the power that the base is being raised to
     * @return the resulting number of base ^ n
     */
    public static int powerN(int base, int n) {
        if (n == 1) {
            return base;
        }
        return base * powerN(base, n - 1);
    }

    /**
     * Recursive method to find the sum of all integers less than or equal to
     * param row.
     * 
     * @param row argument number for base of triangle
     * @return
     */
    public static int triangle(int row) {
        if (row == 0) {
            return 0;
        }
        return row + triangle(row - 1);
    }
}
